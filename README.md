# heapsearch
A repository of heap vulnerabilities on linux with glibc 2.30.
The goal of this repository is to demonstrate different exploits given different combinations of vulnerabilities.
