//created 11:06 3/17/2020
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

char * heapArray[20];
int heapIndices[20];

int editHeap(){
    int index, size, content;
    char buf[8];
    puts("Input an index to edit: ");
    read(0, buf, 8);
    index = atoi(buf);
    puts("Input size of content: ");
    read(0, buf, 8);
    size = atoi(buf);
    puts("Enter content");
    read(0, heapArray[index], size);
    return 1;
}



int createItem(){
    int size, i;
    char buf[14];
    for(i=0; i<20; i++){
        if(heapIndices[i] == 0){
           heapIndices[i] = 1;
            break;
        }
    }
    puts("Enter allocation size: ");
    read(0, buf, 14);
    size = atoi(buf);
    heapArray[i] = (char *) malloc(size);
    puts("Enter content: ");
    read(0, heapArray[i], size);
    return 1;
}

int deleteItem(){
    int index;
    char buf[8];
    puts("Input an index to delete: ");
    read(0, buf, 8);
    index = atoi(buf);
    free(heapArray[index]);
    heapIndices[index] = 0;
    return 1;
}

int viewItem(){
    int index;
    char buf[8];
    puts("Input an index to view: ");
    read(0, buf, 8);
    index = atoi(buf);
    printf("%s\n", heapArray[index]);
    return 1;
}

int main(int argc, char * argv[]){
    int select = 1;
    char buf[8];
    while(select > 0 && select < 5){
        puts("1. New allocation");
        puts("2. Delete allocation");
        puts("3. View allocation");
        puts("4. Change allocation");
        puts("5. Exit");

        read(0, buf, 8);
        select = atoi(buf);
        if(select == 1)
            createItem();
        else if(select == 2)
            deleteItem();
        else if(select == 3)
            viewItem();
        else if(select == 4)
            editHeap();
    }
    puts("Goodbye");
    return 1;
}
